//
// Created by gabriel on 29/03/17.
//

#include <iostream>
#include "RoundRobin.h"
#include "Metrics.h"

#define WAIT_QUEUE 0
#define BLOCK_QUEUE 1

using namespace std;

void printProcessQueue(std::list<Process> queue)
{
    for (std::list<Process>::iterator it=queue.begin(); it != queue.end(); ++it)
        cout << *it << " ";
}

bool compare_processes_block_times(const Process& first, const Process& second) { return (first.getBlockTime() < second.getBlockTime()); }
bool compare_processes_submission_times(const Process& first, const Process& second) { return (first.getSubmissionTime() < second.getSubmissionTime()); }

std::list <Process> removeProcessFromQueue(std::list < Process >* queue, int type) {
    std::list<Process> process2Pop;
    std::list<Process>::iterator i = queue->begin();
    switch (type){
        case WAIT_QUEUE:
            cout << "\t\t\t\t[Start] remove process from WAIT queue main loop" << endl;
            while (i != queue->end()) {
                //cout << "\t\t\t\t[Info] *it points to " << (*i) << endl;
                if (i->getSubmissionTime() == 0) {
                    cout << "\t\t\t\t\t[Info] removing " << (*i) << "from WAIT" << endl;
                    process2Pop.push_back((*i));
                    queue->remove(*i++);
                } else
                    ++i;
            }
            cout << "\t\t\t\t[Done] remove process from WAIT queue main loop" << endl;
            break;
        case BLOCK_QUEUE:
            cout << "\t\t\t\t[Start] remove process from BLOCK queue main loop" << endl;
            while (i != queue->end()) {
                //cout << "\t\t\t\t[Info] *it points to " << (*i) << endl;
                if (i->getBlockTime() == 0) {
                    cout << "\t\t\t\t\t[Info] removing " << (*i) << "from BLOCK" << endl;
                    process2Pop.push_back((*i));
                    queue->remove(*i++);
                } else
                    ++i;
            }
            cout << "\t\t\t\t[Done] remove process from BLOCK queue main loop" << endl;
            break;
    }
        if (!process2Pop.empty())
            return process2Pop;
}

void updateBlockQueue(std::list < Process >* blockQueue)
{
    blockQueue->sort(compare_processes_block_times);

    cout << "\t\t\t\t[Start] updateBlockQueue main loop" << endl;
    std::list<Process>::iterator i = blockQueue->begin();
    while (i != blockQueue->end())
    {
        //cout << "\t\t\t\t\t[Info] *it points to " << (*i) << endl;
        i->setBlockTime(i->getBlockTime() - 1);
        i++;
    }
    cout << "\t\t\t\t[Done] updateBlockQueue main loop" << endl;
}

void updateWaitQueue(std::list < Process >* waitQueue)
{
    waitQueue->sort(compare_processes_block_times);

    cout << "\t\t\t\t[Start] updateWaitQueue main loop" << endl;
    std::list<Process>::iterator i = waitQueue->begin();

    while (i != waitQueue->end())
    {
        //cout << "\t\t\t\t\t[Info] *it points to " << (*i) << endl;
        i->setSubmissionTime(i->getSubmissionTime() - 1);
        //cout << "\t\t\t\t\t[Info] " << (*i) << "-- in WAIT" << endl;
        i++;
    }
    cout << "\t\t\t\t[Done] updateWaitQueue main loop" << endl;
}

void RoundRobin::run(int alpha, int quantum, std::list < Process >* processes)
{
    std::cout << "[Start] "<< "\e[1m" << "Round Robin scheduling" <<"\e[0m" << std::endl;

    int cpuActive = 0,
        originalQuantum = 0,
        totalProcesses = processes->size(),
        waitTimeSum = 0,
        responseTimeSum = 0,
        turnaroundTimeSum = 0,
        cicles = 0;

    std::list <Process> waitQueue,
                        blockQueue,
                        readyQueue,
                        blockedReadyProcess,
                        waitingReadyProcess,
                        runningProcess;

    Process tmp;

    while (!processes->empty())
    {
        tmp = processes->front();
        if (tmp.getSubmissionTime() > 0)
            waitQueue.push_back(tmp);
        else
            readyQueue.push_back(tmp);

        processes->pop_front();
    }

    cout << "\t[Info] process distribution WAIT or READY" << endl;

    waitQueue.sort(compare_processes_submission_times);

    cout << "\t[Info] submission processes sorted" << endl;

    cout << "\t[Start] RoundRobin::run main loop" << endl;
    int sum = 0, diff = 0;
    while (true)
    {
        cout << "\e[1m" << "\t\t[Info] cicle: " << cicles << "\e[0m" << endl;

        sum = blockQueue.size() + readyQueue.size() + runningProcess.size();

        if (!readyQueue.empty() && runningProcess.empty()) {
            runningProcess.push_back((Process &&) readyQueue.front());
            readyQueue.pop_front();
            quantum = originalQuantum;

            if (runningProcess.front().getResponseTime() == -1)
                runningProcess.front().setResponseTime(cicles - runningProcess.front().getInitialSubmissionTime());
        }

        cout << "\t\t\t[Info] running process ";
        if (!runningProcess.empty()) runningProcess.front(); else cout << "-";
        cout << endl;

        cout << "\t\t\t[Info] READY queue ";
        if (!runningProcess.empty())  printProcessQueue(readyQueue); else cout << "-";
        cout << endl;

        cout << "\t\t\t[Info] BLOCK queue ";
        if (!runningProcess.empty()) printProcessQueue(blockQueue); else cout << "-";
        cout << endl;

        cout << "\t\t\t[Info] WAIT queue ";
        if (!runningProcess.empty()) printProcessQueue(waitQueue); else cout << "-";
        cout << endl;

        if (!runningProcess.empty()) {

            if (runningProcess.front().getExecutionTime() > 0)
                runningProcess.front().setExecutionTime(runningProcess.front().getExecutionTime() - 1);

            if (runningProcess.front().getBlockTime() > 0) {
                cout << "\t\t\t[Info]" << runningProcess.front() << "BLOCKED";
                blockQueue.push_back((Process &&) runningProcess.front());

                printProcessQueue(blockQueue); cout << endl;

                runningProcess.pop_front();
                quantum = originalQuantum;
            }
        }

        if (quantum > 0)
            quantum--;

        if (!blockQueue.empty()) {
            cout << "\t\t\t[Info] updating BLOCK queue" << endl;
            blockedReadyProcess = removeProcessFromQueue(&blockQueue, BLOCK_QUEUE);
            updateBlockQueue(&blockQueue);
            while (!blockedReadyProcess.empty()) {
                readyQueue.push_back((Process &&) blockedReadyProcess.front());
                blockedReadyProcess.pop_front();
            }
        }

        if (!waitQueue.empty()) {
            cout << "\t\t\t[Info] updating WAIT queue" << endl;
            waitingReadyProcess = removeProcessFromQueue(&waitQueue, WAIT_QUEUE);
            updateWaitQueue(&waitQueue);

            cout << "\t\t\t[Info] inside waitingReadyProcess ";
            if (!waitingReadyProcess.empty()) waitingReadyProcess.front();
            else cout << "-";
            cout << endl;

            while (!waitingReadyProcess.empty()) {
                diff = alpha - sum;
                if (waitingReadyProcess.size() <= diff && sum < alpha) {
                    readyQueue.push_back((Process &&) waitingReadyProcess.front());
                    waitingReadyProcess.pop_front();
                    cout << "\t\t\t[Info] inside waitingReadyProcess: push_back into READY" << waitingReadyProcess.front() << endl;
                }
                else {
                    for (std::list<Process>::iterator it = waitingReadyProcess.begin(); it != waitingReadyProcess.end(); ++it) {
                        it->setSubmissionTime(1);
                        waitQueue.push_back(*it);
                        if (it == waitingReadyProcess.end()) {
                            waitingReadyProcess.remove(*it);
                            break;
                        } else
                            waitingReadyProcess.remove(*it);
                    }
                }
            }
        }

        if (!runningProcess.empty()
            && runningProcess.front().getExecutionTime() == 0
            && runningProcess.front().getBlockTime() == 0) {

            Metrics::updateProcessMetrics(runningProcess.front(), &waitTimeSum, &responseTimeSum, &turnaroundTimeSum, cicles);

            std::cout << "\e[1m" << "\t\t\t[Info] " << runningProcess.front() << " FINISHED " << "\e[0m";

            Metrics::showMetricsFinishedProcess(runningProcess.front());

            runningProcess.pop_front();
            quantum = originalQuantum;
        }

        if (!runningProcess.empty()
            && !readyQueue.empty()
            && quantum == 0){

            readyQueue.push_back((Process &&) runningProcess.front());
            runningProcess.pop_front();
            quantum = originalQuantum;
        }

        cicles++;

        if (blockQueue.empty() && readyQueue.empty() && waitQueue.empty() && runningProcess.empty())
            break;
    }
    cout << "\t[Done] RoundRobin::run main loop" << endl;

    Metrics::showAlgorithmMetrics(waitTimeSum, responseTimeSum, totalProcesses,
                                  turnaroundTimeSum, cicles, cpuActive);

    std::cout << "[Done] "<< "\e[1m" << "Round Robin scheduling" << "\e[0m" << std::endl;
}
