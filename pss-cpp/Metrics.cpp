//
// Created by gabriel on 29/03/17.
//

#include <tuple>
#include <iostream>
#include "Metrics.h"
#include "Process.h"

void Metrics::showAlgorithmMetrics(int waitTimeSum, int responseTimeSum, int totalProcesses,
                                   int turnaroundTimeSum, int cicles, int cpuActive) {

    int avgTurnaroundTime = waitTimeSum / totalProcesses, 
            avgResponseTime = responseTimeSum / totalProcesses, 
            avgTurnaround = turnaroundTimeSum / totalProcesses;

    std::cout << "Tempo de Resposta Media: "<< avgResponseTime
              << "\nTempo de Espera Media: "<< avgTurnaroundTime
              << "\nTempo de Retorno Medio: "<< avgTurnaround 
              << "\nThroughput: " << totalProcesses / cicles
              << "\nTempo de Servico Medio: "<< (cpuActive / totalProcesses) * 100
              << "\nUtilizacao do Processador: "<< (cpuActive / cicles) * 100 << std::endl;
}

void Metrics::updateProcessMetrics(Process runningProcess, int* waitTimeSum,
                                                         int* responseTimeSum, int* turnaroundTimeSum, int cicles){

    runningProcess.setWaitTime(cicles
                              - runningProcess.getInitialSubmissionTime()
                              - runningProcess.getInitialExecutionTime()
                              - runningProcess.getInitialBlockTime()
                              + 1);

    runningProcess.setTurnaroundTime(runningProcess.getInitialExecutionTime()
                                    + runningProcess.getInitialBlockTime()
                                    + runningProcess.getWaitTime());

    waitTimeSum += runningProcess.getWaitTime();
    responseTimeSum += runningProcess.getResponseTime();
    turnaroundTimeSum += runningProcess.getTurnaroundTime();
}

void Metrics::showMetricsFinishedProcess(Process runningProcess){

    std::cout << "Tempo resp.: " << runningProcess.getResponseTime()
              << ", Tempo esp.: " << runningProcess.getWaitTime()
              << ", Tempo ret.: " << runningProcess.getTurnaroundTime() << std::endl;
}