from queues import atualizar_fila_bloq, atualizar_fila_esp
from metrics import update_info_proc, print_info_finished_proc, print_info_alg

# Algoritmo de prioridade
def algoritmo_prioridade(processos, alfa):

    cpuAtividade = 0
    totalProcessos = len(processos)
    somaTwait = 0
    somaTresponse = 0
    somaTturnaround = 0
    ciclos = 0
    filaEspera = []
    filaBloq = []
    filaPronto = []
    processo_exec = []

    while processos:
        tmp_proc = processos.pop()
        if len(filaPronto) < alfa and tmp_proc.getTsubm() == 0:
            filaPronto.append(tmp_proc)
        else:
            if len(filaPronto) > alfa:
                filaEspera.append(tmp_proc)
                tmp_proc.setTsubm(tmp_proc.getTsubm() + 1)
            if tmp_proc.getTsubm() > 0:
                filaEspera.append(tmp_proc)

    filaPronto.sort(key=lambda a: a.getPrioridade(), reverse=True)
    # print(filaPronto)

    filaEspera.sort(key=lambda a: a.getTsubm(), reverse=False)
    # print(filaEspera)

    while True:

        soma = len(filaPronto) + len(filaBloq) + len(processo_exec)

        # print("\n\nSOMA: ",soma)
        # print("N_CICLOS:", ciclos)
        if filaPronto and (not processo_exec):
            processo_exec.append(filaPronto.pop(0))

            if processo_exec[0].getTresponse() == -1:
                processo_exec[0].setTresponse(ciclos - processo_exec[0].getTSubmInicial())

            if processo_exec[0].getTexec() > 0:
                processo_exec[0].setTexec(processo_exec[0].getTexec() - 1)
            if processo_exec[0].getTbloq() > 0:
                filaBloq.append(processo_exec.pop())

        if filaPronto and processo_exec:
            x = filaPronto[0].getPrioridade()
            y = processo_exec[0].getPrioridade()
            # print("Prioridade do Processo em Execucao:", y)
            # print("Prioridade do Processo do 1º da fila de pronto:", x)
            if x > y:
                filaPronto.append(processo_exec.pop())


        processo_pronto_bloq = atualizar_fila_bloq(filaBloq)
        if processo_pronto_bloq:
            for i in range(len(processo_pronto_bloq)):
                filaPronto.append(processo_pronto_bloq[i])

        processo_pronto_esp = atualizar_fila_esp(filaEspera)
        while processo_pronto_esp:
            diferenca = alfa - soma
            if processo_pronto_esp and len(processo_pronto_esp) <= diferenca and soma < alfa:
                filaPronto.append(processo_pronto_esp.pop())
            else:
                for processo in processo_pronto_esp:
                    if processo.getTsubm == 0:
                        processo.setTsubm(1)
                        processo.setTSubmInicial(processo.getTSubmInicial + 1)
                    filaEspera.append((processo_pronto_esp.pop()))

        if len(filaPronto) > 1:
            filaPronto.sort(key=lambda a: a.getPrioridade(), reverse=True)

        if processo_exec and processo_exec[0].getTexec() == 0 and processo_exec[0].getTbloq() == 0:
            somaTwait, somaTresponse, somaTturnaround = update_info_proc(processo_exec, somaTwait, somaTresponse, somaTturnaround, ciclos)
            print_info_finished_proc(processo_exec)
            print("KILL", processo_exec)
            processo_exec.pop()

        if processo_exec:
            cpuAtividade += 1
            if processo_exec[0].getTexec() > 0:
                processo_exec[0].setTexec(processo_exec[0].getTexec() - 1)
            if processo_exec[0].getTbloq() > 0:
                filaBloq.append(processo_exec.pop())

        ciclos += 1

        if not filaBloq and not filaEspera and not filaPronto and not processo_exec:
            print("Numero de Iteracoes: ", ciclos)
            break

    print_info_alg(somaTwait, somaTresponse, totalProcessos, somaTturnaround, ciclos, cpuAtividade)