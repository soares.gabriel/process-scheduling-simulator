from queues import atualizar_fila_bloq, atualizar_fila_esp
from metrics import update_info_proc, print_info_finished_proc, print_info_alg

"""
Multi-Level Feedback Queue CPU scheduling algorithm

---> com:
            RQ0 q =1
            RQ1 q=2
            RQ2 q=4
            RQ3 q=8

---> um processo não pode retornar aos níveis superiores

Loop:
    IF process exists in blocking queue:
        Work on removing each process from blocking queue in FCFS basis
        When a process is removed, add it back to the highest priority cpu queue

    Move from top priority cpu queue to lowest cpu priority queue until we find a process
    IF a process is found:
        work on process until end of timeslice:
            do work
            IF the process becomes blocked:
                remove it from the cpu queue
                place it on the blocked queue
                restart the timeslice with a new process

        IF the end of the timeslice has been reached:
            remove the process from the top of the queue
            IF the process is not finished:
                IF process is already in lowest priority queue:
                    add process to back of queue
                ELSE:
                    add the process to the back of the next lower priority queue
"""


def insert_processes(processos, filaEspera, filaPronto, alfa=100, gaps=0):
    while processos:

        if gaps == 0:
            tmp_proc = processos.pop()
            if len(filaPronto) < alfa and tmp_proc.getTsubm() == 0:
                filaPronto.append(tmp_proc)
            else:
                filaEspera.append(tmp_proc)
                tmp_proc.setTsubm(tmp_proc.getTsubm() + 1)
        else:
            tmp_proc = processos.pop()
            if tmp_proc.getTsubm() == 0:
                while alfa - gaps != 0:
                    filaPronto.append(tmp_proc)
                    gaps -= 1
            else:
                filaEspera.append(tmp_proc)
                tmp_proc.setTsubm(tmp_proc.getTsubm() + 1)

    filaEspera.sort(key=lambda a: a.getTsubm(), reverse=False)

    return filaEspera, filaPronto


def algoritmo_multilevel_feedback(processos, alfa=100):

    cpuAtividade = 0
    totalProcessos = len(processos)
    somaTwait = 0
    somaTresponse = 0
    somaTturnaround = 0
    ciclos = 0
    quantum0 = 0
    quantum1 = 0
    quantum2 = 0
    quantum3 = 0
    filaEspera = []
    filaBloq = []

    # ReadyQueues
    RQ0 = []
    RQ0_QUANTUM = 1

    RQ1 = []
    RQ1_QUANTUM = 2

    RQ2 = []
    RQ2_QUANTUM = 4

    RQ3 = []
    RQ3_QUANTUM = 8
    processo_exec = []

    filaEspera, RQ0 = insert_processes(processos, filaEspera, RQ0, alfa=alfa)

    while True:

        size = len(RQ0) + len(RQ1) + len(RQ2) + len(RQ3) + len(filaBloq) + 1

        # print("N_CICLO:", ciclos, "SIZE:", size)
        if filaBloq:
            processo_pronto_bloq = atualizar_fila_bloq(filaBloq)
            if processo_pronto_bloq:
                while processo_pronto_bloq:
                    tmp_proc = processo_pronto_bloq.pop()
                    flag = tmp_proc.getFlagBloq()

                    if flag[1] > 0:
                        # print("BLOQ_2_" + flag[0], tmp_proc)
                        if flag[0] == "RQ0":
                            RQ0.append(tmp_proc)
                        elif flag[0] == "RQ1":
                            RQ1.append(tmp_proc)
                        elif flag[0] == "RQ2":
                            RQ2.append(tmp_proc)
                        else:
                            RQ3.append(tmp_proc)
                    else:
                        # print("BLOQ_2_" + flag[0][:-1]+str(int(flag[0][-1]) + 1), tmp_proc)
                        if flag[0] == "RQ0":
                            RQ1.append(tmp_proc)
                        elif flag[0] == "RQ1":
                            RQ2.append(tmp_proc)
                        else:
                            RQ3.append(tmp_proc)

        if filaEspera:
            if size < alfa:
                processo_pronto_esp = atualizar_fila_esp(filaEspera)
                if processo_pronto_esp:
                    while processo_pronto_esp:
                        RQ0.append(processo_pronto_esp.pop())
            else:
                atualizar_fila_esp(filaEspera, delay=True)

        # print("FILA_ESP", len(filaEspera), filaEspera)
        # print("FILA_BLOQ", len(filaBloq), filaBloq)

        if RQ0:
            # print("[DEBUG] RQ0 STARTED with len:", len(RQ0), RQ0)
            if not processo_exec: quantum0 = RQ0_QUANTUM

            if RQ0 and (not processo_exec):
                processo_exec.append(RQ0.pop(0))

                if processo_exec[0].getTresponse() == -1:
                    processo_exec[0].setTresponse(ciclos - processo_exec[0].getTSubmInicial())

            if processo_exec:
                if processo_exec[0].getTexec() > 0:
                    processo_exec[0].setTexec(processo_exec[0].getTexec() - 1)
                    quantum0 -= 1

                # print("PROC_EXEC", processo_exec)
                # print("RQ0_QUANTUM", quantum0)


                if quantum0 == 0 or processo_exec[0].getTexec() == 0:
                    quantum0 = RQ0_QUANTUM
                    if processo_exec[0].getTexec() == 0 and processo_exec[0].getTbloq() == 0:
                        somaTwait, somaTresponse, somaTturnaround = update_info_proc(processo_exec, somaTwait,
                                                                                     somaTresponse, somaTturnaround,
                                                                                     ciclos)
                        print_info_finished_proc(processo_exec)
                        print("FINISHED", processo_exec)
                        processo_exec.pop()
                    elif processo_exec[0].getTbloq() > 0:
                        # print("BLOQ", processo_exec, "for", processo_exec[0].getTbloq(), "I/O cicles  remaining")
                        processo_exec[0].setFlagBloq(["RQ0", quantum0])
                        filaBloq.append(processo_exec.pop())
                    else:
                        # print("GO_DOWN_RQ1", processo_exec, "with", processo_exec[0].getTexec(), "CPU cicles  remaining")
                        RQ1.append(processo_exec.pop())
                        # print("[DEBUG] RQ0 DONE in cicle", ciclos)
        elif RQ1:
            # print("[DEBUG] RQ1 STARTED with len:", len(RQ1), RQ1)
            if not processo_exec: quantum1 = RQ1_QUANTUM

            if RQ1 and (not processo_exec):
                processo_exec.append(RQ1.pop(0))

                if processo_exec[0].getTresponse() == -1:
                    processo_exec[0].setTresponse(ciclos - processo_exec[0].getTSubmInicial())

            if processo_exec:
                if processo_exec[0].getTexec() > 0:
                    processo_exec[0].setTexec(processo_exec[0].getTexec() - 1)
                    quantum1 -= 1

                # print("PROC_EXEC", processo_exec)
                # print("RQ1_QUANTUM", quantum1)


                if quantum1 == 0 or processo_exec[0].getTexec() == 0:
                    quantum1 = RQ1_QUANTUM
                    if processo_exec[0].getTexec() == 0 and processo_exec[0].getTbloq() == 0:
                        somaTwait, somaTresponse, somaTturnaround = update_info_proc(processo_exec, somaTwait,
                                                                                     somaTresponse, somaTturnaround,
                                                                                     ciclos)
                        print_info_finished_proc(processo_exec)
                        print("FINISHED", processo_exec)
                        processo_exec.pop()
                    elif processo_exec[0].getTbloq() > 0:
                        # print("BLOQ", processo_exec, "for", processo_exec[0].getTbloq(), "I/O cicles  remaining")
                        filaBloq.append(processo_exec.pop())
                    else:
                        # print("GO_DOWN_RQ2", processo_exec, "with", processo_exec[0].getTexec(), "CPU cicles  remaining")
                        RQ2.append(processo_exec.pop())
                        # print("[DEBUG] RQ1 DONE in cicle", ciclos)
        elif RQ2:
            # print("[DEBUG] RQ2 STARTED with len:", len(RQ2), RQ2)
            if not processo_exec: quantum2 = RQ2_QUANTUM

            if RQ2 and (not processo_exec):
                processo_exec.append(RQ2.pop(0))

                if processo_exec[0].getTresponse() == -1:
                    processo_exec[0].setTresponse(ciclos - processo_exec[0].getTSubmInicial())

            if processo_exec:
                if processo_exec[0].getTexec() > 0:
                    processo_exec[0].setTexec(processo_exec[0].getTexec() - 1)
                    quantum2 -= 1

                # print("PROC_EXEC", processo_exec)
                # print("RQ2_QUANTUM", quantum2)

                if quantum2 == 0 or processo_exec[0].getTexec() == 0:
                    quantum2 = RQ2_QUANTUM
                    if processo_exec[0].getTexec() == 0 and processo_exec[0].getTbloq() == 0:
                        somaTwait, somaTresponse, somaTturnaround = update_info_proc(processo_exec, somaTwait,
                                                                                     somaTresponse, somaTturnaround,
                                                                                     ciclos)
                        print_info_finished_proc(processo_exec)
                        print("FINISHED", processo_exec)
                        processo_exec.pop()
                    elif processo_exec[0].getTbloq() > 0:
                        # print("BLOQ", processo_exec, "for", processo_exec[0].getTbloq(), "I/O cicles  remaining")
                        filaBloq.append(processo_exec.pop())
                    else:
                        # print("GO_BACK_RQ3", processo_exec, "with", processo_exec[0].getTexec(), "CPU cicles  remaining")
                        RQ3.append(processo_exec.pop())
                        # print("[DEBUG] RQ2 DONE in cicle", ciclos)
        else:
            """
            # Print scheme
            if not RQ0 and not RQ1 and not RQ2 and not RQ3:
                #print("\n[DEBUG] ALL RQs ARE EMPTY", end=' ')
                if filaEspera:
                    print("AWAITING FOR PROCESSES FROM WAIT_Q...\n")
                else:
                    print("\n")
            else:
                print("[DEBUG] RQ3 STARTED with len:", len(RQ3), RQ3)
            """

            if not processo_exec: quantum3 = RQ3_QUANTUM

            if RQ3 and (not processo_exec):
                processo_exec.append(RQ3.pop(0))

                if processo_exec[0].getTresponse() == -1:
                    processo_exec[0].setTresponse(ciclos - processo_exec[0].getTSubmInicial())

            if processo_exec:
                if processo_exec[0].getTexec() > 0:
                    processo_exec[0].setTexec(processo_exec[0].getTexec() - 1)
                    quantum3 -= 1

                # print("PROC_EXEC", processo_exec)
                # print("RQ3_QUANTUM", quantum3)

                if quantum3 == 0 or processo_exec[0].getTexec() == 0:
                    quantum3 = RQ3_QUANTUM
                    if processo_exec[0].getTexec() == 0 and processo_exec[0].getTbloq() == 0:
                        somaTwait, somaTresponse, somaTturnaround = update_info_proc(processo_exec, somaTwait,
                                                                                     somaTresponse, somaTturnaround,
                                                                                     ciclos)
                        print_info_finished_proc(processo_exec)
                        print("FINISHED", processo_exec)
                        processo_exec.pop()
                    elif processo_exec[0].getTbloq() > 0:
                        # print("BLOQ", processo_exec, "for", processo_exec[0].getTbloq(), "I/O cicles  remaining")
                        filaBloq.append(processo_exec.pop())
                    else:
                        # print("GO_BACK_RQ3", processo_exec, "with", processo_exec[0].getTexec(), "CPU cicles  remaining")
                        RQ3.append(processo_exec.pop())
                        # print("[DEBUG] RQ3 DONE in cicle", ciclos)

        ciclos += 1

        if not RQ0 and not RQ1 and not RQ2 and not RQ3 and not filaBloq and not filaEspera and not processo_exec:
            break

    print("N_CICLOS_TOTAL:", ciclos)
    print_info_alg(somaTwait, somaTresponse, totalProcessos, somaTturnaround, ciclos, cpuAtividade)