from queues import atualizar_fila_bloq, atualizar_fila_esp
from metrics import update_info_proc, print_info_finished_proc, print_info_alg


def algoritmo_stateDependent(processos, alfa):

    cpuAtividade = 0
    totalProcessos = len(processos)
    somaTwait = 0
    somaTresponse = 0
    somaTturnaround = 0
    ciclos = 0
    quantum = 0
    filaEspera = []
    filaBloq = []
    filaPronto = []
    processo_exec = []

    while processos:
        tmp_proc = processos.pop()
        if len(filaPronto) < alfa and int(tmp_proc.getTsubm()) == 0:
            filaPronto.append(tmp_proc)
        else:
            filaEspera.append(tmp_proc)
            tmp_proc.setTsubm(tmp_proc.getTsubm() + 1)

    filaEspera.sort(key=lambda a: a.getTsubm(), reverse=False)

    while True:

        soma = len(filaPronto) + len(filaBloq) + len(processo_exec)

        # print("\n\nSOMA: ", soma)
        # print("N_CICLOS:", ciclos)
        if filaPronto and (not processo_exec):
            processo_exec.append(filaPronto.pop(0))

            if len(filaPronto) > 0:
                quantum = int(alfa / len(filaPronto))

            if processo_exec[0].getTresponse() == -1:
                processo_exec[0].setTresponse(ciclos - processo_exec[0].getTSubmInicial())

        # print("QUANTUM: ", quantum)
        # print("FILA_PRONTO", "TAM:", len(filaPronto))
        # print("FILA_ESP", "TAM:", len(filaEspera))
        # print("FILA_BLOQ", "TAM:", len(filaBloq))
        # print("PROC_EXEC", processo_exec)

        if processo_exec:
            if processo_exec[0].getTexec() > 0:
                processo_exec[0].setTexec(processo_exec[0].getTexec() - 1)
            if processo_exec[0].getTbloq() > 0:
                filaBloq.append(processo_exec.pop())
                quantum = 0

        if quantum > 0:
            quantum -= 1

        if processo_exec and processo_exec[0].getTexec() == 0 and processo_exec[0].getTbloq() == 0:
            somaTwait, somaTresponse, somaTturnaround = update_info_proc(processo_exec, somaTwait, somaTresponse,
                                                                         somaTturnaround, ciclos)
            print_info_finished_proc(processo_exec)
            print("KILL", processo_exec)
            processo_exec.pop()

        if processo_exec and filaPronto and quantum <= 0:
            filaPronto.append(processo_exec.pop())

        processo_pronto_bloq = atualizar_fila_bloq(filaBloq)
        if processo_pronto_bloq:
            for i in range(len(processo_pronto_bloq)):
                filaPronto.append(processo_pronto_bloq[i])

        processo_pronto_esp = atualizar_fila_esp(filaEspera)
        while processo_pronto_esp:
            diferença = alfa - soma
            if processo_pronto_esp and len(processo_pronto_esp) <= diferença and soma < alfa:
                filaPronto.append(processo_pronto_esp.pop())
            else:
                for processo in processo_pronto_esp:
                    if processo.getTsubm == 0:
                        processo.setTsubm(1)
                    filaEspera.append((processo_pronto_esp.pop()))

        ciclos += 1

        if not filaBloq and not filaEspera and not filaPronto and not processo_exec:
            break

    print_info_alg(somaTwait, somaTresponse, totalProcessos, somaTturnaround, ciclos, cpuAtividade)