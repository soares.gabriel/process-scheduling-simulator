def print_info_alg(somaTwait, somaTresponse, totalProcessos, somaTturnaround, ciclos, cpuAtividade):
    mediaTwait = somaTwait / totalProcessos
    mediaTresponse = somaTresponse / totalProcessos
    mediaTturnaround = somaTturnaround / totalProcessos
    print("Tempo de Resposta Media: ", mediaTresponse)
    print("Tempo de Espera Media: ", mediaTwait)
    print("Tempo de Retorno Medio: ", mediaTturnaround)
    print("Throughput: ", totalProcessos / ciclos)
    print("Tempo de Servico Medio: ", (cpuAtividade / totalProcessos) * 100)
    print("Utilizacao do Processador: ", (cpuAtividade / ciclos) * 100)


def update_info_proc(processo_exec, somaTwait, somaTresponse, somaTturnaround, ciclos):
    processo_exec[0].setTwait(
        ciclos - processo_exec[0].getTSubmInicial() - processo_exec[0].getTExecInicial() - processo_exec[
            0].getTBloqInicial() + 1)
    processo_exec[0].setTturnaround(
        processo_exec[0].getTExecInicial() + processo_exec[0].getTBloqInicial() + processo_exec[0].getTwait())
    somaTwait += processo_exec[0].getTwait()
    somaTresponse += processo_exec[0].getTresponse()
    somaTturnaround += processo_exec[0].getTturnaround()

    return somaTwait, somaTresponse, somaTturnaround

def print_info_finished_proc(processo_exec):

    print("PID "+str(processo_exec[0].getId())+
          ", Tempo resp.: "+str(processo_exec[0].getTresponse())+
          ", Tempo esp.: "+str(processo_exec[0].getTwait())+
          ", Tempo ret.: "+str(processo_exec[0].getTturnaround()))
    # print("Tempo de Resposta de P", processo_exec[0].getId(), ": ", processo_exec[0].getTresponse())
    # print("Tempo de Espera de P", processo_exec[0].getId(), ":", processo_exec[0].getTwait())
    # print("Tempo de Retorno de P", processo_exec[0].getId(), ":", processo_exec[0].getTturnaround())