# def atualizar_fila_esp(fila):
#     # print("Atualizando fila ESPERA")
#
#     fila.sort(key=lambda a: a.getTsubm(), reverse=False)
#
#     proc_a_remover = []
#
#     i = 0
#     size = len(fila)
#     while True:
#
#         if i >= size:
#             break
#
#         if fila[i].getTsubm() == 0:
#             proc_a_remover.append(fila.pop(i))
#             size -= 1
#         else:
#             fila[i].setTsubm(fila[i].getTsubm() - 1)
#             i += 1
#
#     # print("OK")
#     if proc_a_remover:
#         return proc_a_remover


def atualizar_fila_bloq(fila):
    # print("Atualizando fila BLOQUEIO...")

    fila.sort(key=lambda a: a.getTbloq(), reverse=True)

    proc_a_remover = []

    i = 0
    size = len(fila)
    while True:

        if i >= size:
            break

        if fila[i].getTbloq() == 0:
            proc_a_remover.append(fila.pop(i))
            size -= 1
        else:
            fila[i].setTbloq(fila[i].getTbloq() - 1)
            i += 1

    # print("OK")
    if proc_a_remover:
        return proc_a_remover

def atualizar_fila_esp(fila, delay=False):
    # print("Updating WAIT_Q...", end='')

    if not delay:
        fila.sort(key=lambda a: a.getTsubm(), reverse=False)

        proc_a_remover = []

        i = 0
        size = len(fila)
        while True:

            if i >= size:
                break

            if fila[i].getTsubm() == 0:
                proc_a_remover.append(fila.pop(i))
                size -= 1
            else:
                fila[i].setTsubm(fila[i].getTsubm() - 1)
                i += 1
        # print("OK --")
        if proc_a_remover:
            return proc_a_remover
    else:
        for process in fila:
            process.setTsubm(process.getTsubm() + 1)
            # print("OK ++")

