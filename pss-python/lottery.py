import random
import numpy
from queues import atualizar_fila_bloq, atualizar_fila_esp
from metrics import update_info_proc, print_info_finished_proc, print_info_alg

"""
Lottery scheduling (Waldspurger and Weihl, 1994)

The basic idea is to give processes lottery tickets for various system resources,
such as CPU time. Whenever a scheduling decision has to be made, a lottery ticket
is chosen at random, and the process holding that ticket gets the resource. When
applied to CPU scheduling, the system might hold a lottery 50 times a second, with
each winner getting 20 msec of CPU time as a prize.

More important processes can be given extra tickets, to increase
their odds of winning. If there are 100 tickets outstanding, and one process holds
20 of them, it will have a 20% chance of winning each lottery. In the long run, it
will get about 20% of the CPU. In contrast to a priority scheduler, where it is very
hard to state what having a priority of 40 actually means, here the rule is clear: a
process holding a fraction f of the tickets will get about a fraction f of the resource
in question.

Lottery scheduling has several interesting properties. For example, if a new
process shows up and is granted some tickets, at the very next lottery it will have a
chance of winning in proportion to the number of tickets it holds. In other words,
lottery scheduling is highly responsive.

# Papers

Lottery Scheduling: Flexible Proportional-Share Resource Management by Carl A. Waldspurger
and William E. Weihl. The 1994 Operating Systems Design and Implementation conference
(OSDI '94). November, 1994. Monterey, California.
https://www.usenix.org/legacy/publications/library/proceedings/osdi/full_papers/waldspurger.pdf

Implementing Lottery Scheduling - Matching the Specialization in Traditional Schedulers -
Paper by David Petrou et al. https://www.usenix.org/legacy/events/usenix99/full_papers/petrou/petrou.pdf

"""

random.seed(10)  # definir se for necessaria a reproducao

def ticket_dist(processos):
    ticket_list = []

    # distribuicao dos tickets
    for i in range(0, len(processos)):
        ticket = (processos[i].getPrioridade() + 1) * 10
        processos[i].setTicket(ticket)
        ticket_list.append(ticket)

        ## print(processos_exec)
    return max(ticket_list), min(ticket_list)


def get_lucky_process(processos):
    ticket_max, ticket_min = ticket_dist(processos)
    sorted_ticket = random.randint(ticket_min, ticket_max)
    lucky_process = min(processos, key=lambda x: (x.getTicket() - sorted_ticket))

    if lucky_process in processos:
        processos.remove(lucky_process)

    return lucky_process, processos


def redist_tickets(processo, processos):
    for i in random.sample(range(1, len(processos)), len(processos) // 2):
        processos[i].setTicket(processo.getTicket() + numpy.ceil(processo.getTicket() / len(processos)))
    return processos


def insert_processes(processos, filaEspera, filaPronto, alfa=100, gaps=0):
    while processos:

        if gaps == 0:
            tmp_proc = processos.pop()
            if len(filaPronto) < alfa and tmp_proc.getTsubm() == 0:
                filaPronto.append(tmp_proc)
            else:
                filaEspera.append(tmp_proc)
                tmp_proc.setTsubm(tmp_proc.getTsubm() + 1)
        else:
            tmp_proc = processos.pop()
            if tmp_proc.getTsubm() == 0:
                while alfa - gaps != 0:
                    filaPronto.append(tmp_proc)
                    gaps -= 1
            else:
                filaEspera.append(tmp_proc)
                tmp_proc.setTsubm(tmp_proc.getTsubm() + 1)

    filaEspera.sort(key=lambda a: a.getTsubm(), reverse=False)

    return filaEspera, filaPronto


def algoritmo_lottery(processos, alfa=100, quantum=2):

    cpuAtividade = 0
    totalProcessos = len(processos)
    somaTwait = 0
    somaTresponse = 0
    somaTturnaround = 0
    ciclos = 0
    filaEspera = []
    filaBloq = []
    filaPronto = []
    processo_exec = []
    quantum0 = 0

    filaEspera, filaPronto = insert_processes(processos, filaEspera, filaPronto, alfa=alfa)

    while True:

        size = len(filaPronto) + len(filaBloq) + 1
        # print("N_CICLO:", ciclos, "SIZE:", size)

        if filaBloq:
            processo_pronto_bloq = atualizar_fila_bloq(filaBloq)
            if processo_pronto_bloq:
                while processo_pronto_bloq:
                    filaPronto.append(processo_pronto_bloq.pop())

        if filaEspera:
            if size < alfa:
                processo_pronto_esp = atualizar_fila_esp(filaEspera)
                if processo_pronto_esp:
                    while processo_pronto_esp:
                        filaPronto.append(processo_pronto_esp.pop())
            else:
                atualizar_fila_esp(filaEspera, delay=True)

        if filaPronto:
            # print("[DEBUG] READY_Q STARTED with len:", len(filaPronto), filaPronto)
            if not processo_exec: quantum0 = quantum

            if filaPronto and (not processo_exec):
                tmp_proc, filaPronto = get_lucky_process(filaPronto)
                processo_exec.append(tmp_proc)
                quantum0 *= tmp_proc.getTicket()

                if processo_exec[0].getTresponse() == -1:
                    processo_exec[0].setTresponse(ciclos - processo_exec[0].getTSubmInicial())

            if processo_exec:
                if processo_exec[0].getTexec() > 0:
                    processo_exec[0].setTexec(processo_exec[0].getTexec() - 1)
                    quantum0 -= 1

                # print("PROC_EXEC", processo_exec, "TICKETS:", processo_exec[0].getTicket(), "QUANTUM", quantum0)


                if quantum0 == 0 or processo_exec[0].getTexec() == 0:
                    quantum0 = quantum
                    if processo_exec[0].getTexec() == 0 and processo_exec[0].getTbloq() == 0:
                        # print("FINISHED", processo_exec)
                        somaTwait, somaTresponse, somaTturnaround = update_info_proc(processo_exec, somaTwait,
                                                                                     somaTresponse, somaTturnaround,
                                                                                     ciclos)
                        print_info_finished_proc(processo_exec)
                        print("KILL", processo_exec)
                        processo_exec.pop()
                    elif processo_exec[0].getTbloq() > 0:
                        # print("BLOQ", processo_exec, "for", processo_exec[0].getTbloq(), "I/O cicles  remaining")
                        filaPronto = redist_tickets(processo_exec[0], filaPronto)
                        filaBloq.append(processo_exec.pop())
                    else:
                        # print("GO_BACK_READY_Q", processo_exec, "with", processo_exec[0].getTexec(), "CPU cicles  remaining")
                        filaPronto.append(processo_exec.pop())
        else:
            if processo_exec:
                if processo_exec[0].getTexec() > 0:
                    processo_exec[0].setTexec(processo_exec[0].getTexec() - 1)
                    quantum0 -= 1
                # print("PROC_EXEC", processo_exec, "TICKETS:", processo_exec[0].getTicket(), "QUANTUM", quantum0)
                if quantum0 == 0 or processo_exec[0].getTexec() == 0:
                    quantum0 = quantum
                    if processo_exec[0].getTexec() == 0 and processo_exec[0].getTbloq() == 0:
                        # print("FINISHED", processo_exec)
                        processo_exec.pop()

        ciclos += 1

        if not filaBloq and not filaEspera and not filaPronto and not processo_exec:
            break

            # print("N_CICLOS_TOTAL:", ciclos)
    print_info_alg(somaTwait, somaTresponse, totalProcessos, somaTturnaround, ciclos, cpuAtividade)