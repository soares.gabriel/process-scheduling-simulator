from queues import atualizar_fila_bloq, atualizar_fila_esp
from metrics import update_info_proc, print_info_finished_proc, print_info_alg

# def atualizar_fila_esp(fila):
#     # print("Atualizando fila ESPERA")
#
#     fila.sort(key=lambda a: a.getTsubm(), reverse=False)
#
#     proc_a_remover = []
#
#     i = 0
#     size = len(fila)
#     while True:
#
#         if i >= size:
#             break
#
#         if fila[i].getTsubm() == 0:
#             proc_a_remover.append(fila.pop(i))
#             size -= 1
#         else:
#             fila[i].setTsubm(fila[i].getTsubm() - 1)
#             i += 1
#
#     # print("OK")
#     if proc_a_remover:
#         return proc_a_remover
#
#
# def atualizar_fila_bloq(fila):
#     # print("Atualizando fila BLOQUEIO...")
#
#     fila.sort(key=lambda a: a.getTbloq(), reverse=True)
#
#     proc_a_remover = []
#
#     i = 0
#     size = len(fila)
#     while True:
#
#         if i >= size:
#             break
#
#         if fila[i].getTbloq() == 0:
#             proc_a_remover.append(fila.pop(i))
#             size -= 1
#         else:
#             fila[i].setTbloq(fila[i].getTbloq() - 1)
#             i += 1
#
#     # print("OK")
#     if proc_a_remover:
#         return proc_a_remover


def calcular_ratio(fila, totalExec):
    for i in range(len(fila)):
        if fila[i].getTwait() > 0:
            tSubm = fila[i].getTwait()
            fila[i].setTwait(totalExec - tSubm)
        else:
            fila[i].setTwait(totalExec)

        # if fila[i].getTexec() == 0 and fila[i].getTbloq() == 0:
        #     fila[i].setRatio(fila[i].getTwait())
        # else:
        fila[i].setRatio((fila[i].getTwait() + fila[i].getTtotal()) / fila[i].getTtotal())


def algoritmo_hrrn(processos, alfa):

    cpuAtividade = 0
    totalProcessos = len(processos)
    somaTwait = 0
    somaTresponse = 0
    somaTturnaround = 0
    ciclos = 0
    totalExec = 0
    filaEspera = []
    filaBloq = []
    filaPronto = []
    processo_exec = []


    while processos:
        tmp_proc = processos.pop()
        if len(filaPronto) < alfa and int(tmp_proc.getTsubm()) == 0:
            filaPronto.append(tmp_proc)
        elif len(filaPronto) == alfa and int(tmp_proc.getTsubm()) == 0:
            filaEspera.append(tmp_proc)
            tmp_proc.setTsubm(tmp_proc.getTsubm() + 1)
            tmp_proc.setTwait(tmp_proc.getTsubm())
        else:
            filaEspera.append(tmp_proc)
            tmp_proc.setTwait(tmp_proc.getTsubm())

    filaEspera.sort(key=lambda a: a.getTsubm(), reverse=False)


    while True:

        soma = len(filaPronto) + len(filaBloq) + len(processo_exec)

        # print("\n\nSOMA:", soma)
        # print("N_CICLOS:", ciclos)
        if filaPronto and (not processo_exec):
            processo_exec.append(filaPronto.pop(0))  # remove o processo com prioridade mais alta

            if processo_exec[0].getTresponse() == -1:
                processo_exec[0].setTresponse(ciclos - processo_exec[0].getTSubmInicial())

            if processo_exec[0].getTbloq() > 0:
                totalExec += 1
            else:
                totalExec += processo_exec[0].getTexec()

        if processo_exec:
            if processo_exec[0].getTexec() > 0:
                processo_exec[0].setTexec(processo_exec[0].getTexec() - 1)

            if processo_exec[0].getTbloq() > 0:
                filaBloq.append(processo_exec.pop())

        if processo_exec and processo_exec[0].getTexec() == 0 and processo_exec[0].getTbloq() == 0:
            print("KILL", processo_exec)
            somaTwait, somaTresponse, somaTturnaround = update_info_proc(processo_exec, somaTwait, somaTresponse,
                                                                         somaTturnaround, ciclos)
            print_info_finished_proc(processo_exec)
            processo_exec.pop()

        processo_pronto_bloq = atualizar_fila_bloq(filaBloq)
        if processo_pronto_bloq:
            for i in range(len(processo_pronto_bloq)):
                filaPronto.append(processo_pronto_bloq[i])

        processo_pronto_esp = atualizar_fila_esp(filaEspera)
        while processo_pronto_esp:
            diferença = alfa - soma
            if processo_pronto_esp and len(processo_pronto_esp) <= diferença and soma < alfa:
                filaPronto.append(processo_pronto_esp.pop())
            else:
                for processo in processo_pronto_esp:
                    if processo.getTsubm == 0:
                        processo.setTsubm(1)
                        processo.setTwait(processo.getTwait() + 1)
                    filaEspera.append((processo_pronto_esp.pop()))

        if len(filaPronto) > 1:
            calcular_ratio(filaPronto, totalExec)
            filaPronto.sort(key=lambda a: a.getRatio(), reverse=True)

        ciclos += 1

        if not filaBloq and not filaEspera and not filaPronto and not processo_exec:
            break

    print_info_alg(somaTwait, somaTresponse, totalProcessos, somaTturnaround, ciclos, cpuAtividade)
