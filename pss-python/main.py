__author__ = ["Gabriel Soares", "Rodrigo Akio Otsuka"]
__contact__ = ["gabriel.soares@dcomp.ufs.br", "rodrigo.otsuka@dcomp.ufs.br"]
__copyright__ = "Copyright (c) 2016-2017 Gabriel Soares & Rodrigo Otsuka"
__date__ = "2017-02-03"
__version__ = "0.1"

import time, copy
from prioridade import algoritmo_prioridade
from hrrn import algoritmo_hrrn
from multilevelfeedback import algoritmo_multilevel_feedback
from roundrobin import algoritmo_roundRobin
from statedependent import algoritmo_stateDependent
from lottery import algoritmo_lottery
from processo import Processo

arq = open('processos/cenario1.txt', 'r')
texto = arq.readlines()
processos = []

alfa = 100

tWait = 0
ratio = 0
response = -1
turnaround = 0
tExecInicial = 0
tBloqInicial = 0
tSubmInicial = 0

for linha in texto:
    id, tSubm, prioridade, tExec, tBloq = linha.split()
    # print("P"+id, prioridade, tExec, tBloq, tSubm)
    processos.append(
        Processo(id, tSubm, prioridade, tExec, tBloq, tWait, ratio, response, turnaround, tExecInicial, tBloqInicial,
                 tSubmInicial))

print("# Round Robin #")
start = time.time()
algoritmo_roundRobin(processos, alfa, quantum=4)
end = time.time()
elapsed = end - start
print("# Round Robin # Elapsed time:", elapsed)

print("# Prioridade #")
start = time.time()
algoritmo_prioridade(processos, alfa)
end = time.time()
elapsed = end - start
print("# Prioridade # Elapsed time:", elapsed)

print("# HRRN #")
start = time.time()
algoritmo_hrrn(processos, alfa)
end = time.time()
elapsed = end - start
print("# HRRN # Elapsed time:", elapsed)

print("# State Dependent #")
start = time.time()
algoritmo_stateDependent(processos, alfa)
end = time.time()
elapsed = end - start
print("# State Dependent # Elapsed time:", elapsed)

print("# Lottery #")
start = time.time()
algoritmo_lottery(processos, alfa, quantum=2)
end = time.time()
elapsed = end - start
print("# Lottery # Elapsed time:", elapsed)

print("# Multilevel Feedback #")
start = time.time()
algoritmo_multilevel_feedback(processos, alfa)
end = time.time()
elapsed = end - start
print("# Multilevel Feedback # Elapsed time:", elapsed)
