### Estrutura dos arquivos

| Nome | Prioridade | Ciclos de CPU | Ciclos de I/O | Submissão |
|------|------------|---------------|---------------|-----------|
|  P1  |     1      |      10       |     2         |    0      |
| ...  |   ...      |    ...        |    ...        |   ...     |
| ...  |   ...      |    ...        |    ...        |   ...     |
