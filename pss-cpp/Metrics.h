//
// Created by gabriel on 29/03/17.
//

#ifndef SCHEDULING_METRICS_H
#define SCHEDULING_METRICS_H


#include "Process.h"

class Metrics {
public:
    static void
    showMetricsFinishedProcess(Process);

    static void
    updateProcessMetrics(Process, int*, int*, int*, int);

    static void
    showAlgorithmMetrics(int, int, int, int, int, int);
private:
    Metrics();
};


#endif //SCHEDULING_METRICS_H
