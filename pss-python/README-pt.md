# Sistemas Operacionais - 2016.2
## Projeto 1 - Algoritmos de Escalonamento

Neste projeto são implementados 6 (seis) algoritmos de escalonamento de CPU dos que estão listados abaixo e são executadas simulações para alguns cenários que serão descritos em seguida. Ao final das simulações, algumas informações baseadas nos resultados encontrados são calculadas.

Algoritmos de escalonamento:
- RR: Round Robin (com quantum = 2, 4)
- **HRRN: Highest Response Ratio Next**
- **Multilevel feedback**
- Priority scheduling
- **Lottery scheduling**
- **State-dependent priority methods**

### Métricas de desempenho

Para cada algoritmo, as seguintes métricas são calculadas para cada processo:

- Tempo de retorno (turnaround time)
- Tempo de resposta (response time)
- Tempo de espera (waiting time)

Ao final da simulação para cada cenário e algoritmo, as seguintes informações são exibidas em um arquivo de saída:

- Tempo de retorno médio
- Tempo de serviço médio
- Utilização do processador
- Tempo de resposta médio
- Throughput
- Tempo de espera médio
- Duração da simulação

### Cenários de simulação
Os algoritmos são executados utilizando 5 (cinco) cenários contendo conjuntos de processos diferentes. Cada processo possui:

- Identificador único;
- Tempo de submissão;
- Prioridade;
- Tempo para execução;
- Tempo de bloqueio.

A qualquer instante de tempo durante a simulação, o grau de multiprogramação do sistema é de α processos, ou seja, no máximo α processos são criados. As simulações devem ser feitas para α = 100 e 200. Cinco cenários são testados e os arquivos de entrada são fornecidos.

- Cenário 1: 1.000 processos
- Cenário 2: 5.000 processos
- Cenário 3: 10.000 processos
- Cenário 4: 20.000 processos
- Cenário 5: 50.000 processos

### Resultados

Métricas

|  Algoritmo  | Avg. turnaround | Avg. service time | Processor utilization | Avg. response time | Avg. waiting time | Throughput |
|:-----------:|:---------------:|:-----------------:|:---------------------:|:------------------:|:-----------------:|:----------:|
|     FIFO    |                 |                   |                       |                    |                   |            |
|      RR     |                 |                   |                       |                    |                   |            |
|     SJF     |                 |                   |                       |                    |                   |            |
| Prioridades |                 |                   |                       |                    |                   |            |
|     VRR     |                 |                   |                       |                    |                   |            |
