class Processo(object):

    def __init__(self, id, tSubm, prioridade, tTotal, tBloq ,tWait, ratio, response, turnaround, tExecInicial, tBloqInicial, tSubmInicial):
        self._id = int(id)
        self._tSubm = int(tSubm)
        self._prioridade = int(prioridade)
        self._tExec = int(tTotal) - int(tBloq)
        self._tTotal = int(tTotal)
        self._tBloq = int(tBloq)
        self._tWait = int(tWait)            #Tempo de espera: Todo aquele tempo que o processo não está executando (Nem cpu, nem io)
        self._ratio = int(ratio)
        self._tResponse = int(response)     #Tempo de resposta: Tempo até que o processo seja executado pela primeira vez
        self._tTurnaround = int(turnaround) #Tempo de serviço ou retorno: Tempo de espera + Tempo Total(CPU & IO)
        self._tExecInicial = int(tTotal) - int(tBloq)
        self._tBloqInicial = int(tBloq)
        self._tSubmInicial = int(tSubm)
        self._flagBloq = False
        self._tickets = None

    def __str__(self):
        if self._ratio == 0 and self._tWait == 0:
            return "<PID:"+str(self._id)+" TS:"+str(self._tSubm)\
                   +" PR:"+str(self._prioridade)+" TE:"+str(self._tExec)+\
                   " TB:"+str(self._tBloq)+">"
        else:
            return "<PID:" + str(self._id) + " TS:" + str(self._tSubm) \
                   + " PR:" + str(self._prioridade) + " TE:" + str(self._tExec) + \
                   " TB:" + str(self._tBloq) + \
                   " TW:"+ str(self._tWait) + " RATIO:" + str(self._ratio) + ">"

    def __repr__(self):
        return str(self)

    def getId(self):
        return self._id

    def setId(self, valor):
        self._id = valor

    def getTsubm(self):
        return self._tSubm

    def setTsubm(self, valor):
        self._tSubm = valor

    def getPrioridade(self):
        return self._prioridade

    def setPrioridade(self, valor):
        self._prioridade = valor

    def getTexec(self):
        return self._tExec

    def setTexec(self, valor):
        self._tExec = valor

    def getTbloq(self):
        return self._tBloq

    def setTbloq(self, valor):
        self._tBloq = valor

    def setTwait(self, valor):
        self._tWait = valor

    def getTwait(self):
        return self._tWait

    def setRatio(self, valor):
        self._ratio = valor

    def getRatio(self):
        return self._ratio

    def getFlagBloq(self):
        return self._flagBloq

    def setFlagBloq(self, valor):
        self._flagBloq = valor

    def getTtotal(self):
        return self._tTotal

    def setTresponse(self, valor):
        self._tResponse = valor

    def getTresponse(self):
        return self._tResponse

    def setTturnaround(self, valor):
        self._tTurnaround = valor

    def getTturnaround(self):
        return self._tTurnaround

    def getTExecInicial(self):
        return self._tExecInicial

    def getTBloqInicial(self):
        return self._tBloqInicial

    def getTSubmInicial(self):
        return self._tSubmInicial

    def setTicket(self, valor):
        self._tickets = valor

    def getTicket(self):
        return self._tickets