//
// Created by gabriel on 29/03/17.
//

#ifndef SCHEDULING_ROUNDROBIN_H
#define SCHEDULING_ROUNDROBIN_H

#include <list>
#include "Process.h"

class RoundRobin {
public:
    static void run(int alpha, int quantum, std::list < Process >* processes);
private:
    RoundRobin();
};


#endif //SCHEDULING_ROUNDROBIN_H
