#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <list>
#include <algorithm>

#include "Process.h"
#include "fast-cpp-csv-parser/csv.h"        /* https://github.com/ben-strasser/fast-cpp-csv-parser */
#include "RoundRobin.h"

using namespace std;

void help()
{
    cout << "\n\t# Simulador de algoritmos de escalonamento #\n\tUso: programa [INPUT FILE] [OUTPUT FILE]\n\n";
}

int main(int argc, char **argv) {

    list < Process > proc_data;                                  /* processes from file */
    int ID, submission_time, priority, total_time, block_time;      /* process info */

    if ( argc != 3 ){                                               /* argc=3 p/ correta execucao do programa */
        help();
    } else {

        ifstream input(argv[1]);
        ofstream output(argv[2]);


        if ( !input.is_open() && !output.is_open())                 /* checar se os arquivos foram abertos devidademente */
            cout <<"\n\nNao foi possivel abrir o(s) arquivo(s) de entrada e/ou saida.\n\n";
        else {
            io::CSVReader<5> in(argv[1]);
            while(in.read_row(ID, submission_time, priority, total_time, block_time)){

                Process tmp(ID, priority, submission_time, block_time, total_time);
                proc_data.push_back(tmp);
                //cout << tmp << endl;
            }
            cout << "[Info]\e[1m"<<" All processes LOADED from: "<<argv[1] <<"\e[0m" << endl;
            RoundRobin::run(100, 2, &proc_data);

        }
    }

    return EXIT_SUCCESS;

}
